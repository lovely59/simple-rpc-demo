package com.lyon.demo.server;

import cn.hutool.aop.ProxyUtil;
import cn.hutool.json.JSONUtil;
import com.lyon.demo.example.service.HelloService;

/**
 * @author Lyon
 */
public class RpcProxyDemo {

    public static void main(String[] args) {
        Class<HelloService> serviceClass = HelloService.class;
        HelloService helloService = ProxyUtil.newProxyInstance((o, method, objects) -> {
            System.out.println(JSONUtil.toJsonStr(objects));
            return "实打实大";
        }, serviceClass);
        helloService.hello("123");
    }
}

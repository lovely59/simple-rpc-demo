package com.lyon.demo.server;

import com.lyon.demo.common.spi.DefaultSpiLoader;
import com.lyon.demo.example.service.HelloService;
import com.lyon.demo.rpc.api.endpoint.RpcAccessEndpoint;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.Properties;

/**
 * @author Lyon
 */
@Slf4j
public class RpcClient {

    private static final String PROTOCOL = "netty";

    public static void main(String[] args) throws Exception {
        // 创建远程访问服务 （客户端，服务端，命名服务自动注册）
        final RpcAccessEndpoint rpcAccessEndpoint = DefaultSpiLoader.loader(RpcAccessEndpoint.class, PROTOCOL);
        Properties prop = initProp();
        rpcAccessEndpoint.startServer(prop);
        final HelloService helloService = rpcAccessEndpoint.getRemoteService(HelloService.class);
        while (true){
            Thread.sleep(3000);
            final String result = helloService.hello("张三");
            log.info("远程调用结果 {}",result);
        }
    }

    private static Properties initProp() {
        final Properties prop = new Properties();
        prop.setProperty("remote-protocol",PROTOCOL);
        prop.setProperty("naming-Protocol",PROTOCOL);
        prop.setProperty("naming-remote-protocol",PROTOCOL);
        prop.setProperty("serviceName","rpc-client-demo");
        prop.put("naming-addr", new InetSocketAddress("localhost",18880));
        prop.put("port",18882);
        return prop;
    }

}

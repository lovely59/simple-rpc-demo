package com.lyon.demo.rpc.naming.core;

import com.lyon.demo.netty.core.bytebuf.CommandDecoder;
import com.lyon.demo.netty.core.bytebuf.CommandEncoder;
import com.lyon.demo.rpc.naming.dispatcher.DispatcherHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPromise;
import lombok.AllArgsConstructor;


/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@AllArgsConstructor
public class ChildChannelHandler extends ChannelInitializer<Channel> {

    private final DispatcherHandler dispatcherHandler;

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline()
                .addLast(new CommandDecoder())
                .addLast(dispatcherHandler)
                .addLast(new CommandEncoder())
                ;
    }
}

package com.lyon.demo.rpc.naming.dispatcher.handler;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Pair;
import cn.hutool.core.util.SerializeUtil;
import com.lyon.demo.rpc.api.core.Command;
import com.lyon.demo.rpc.api.core.Result;
import com.lyon.demo.rpc.api.core.ServiceInstance;
import com.lyon.demo.rpc.api.core.Types;
import com.lyon.demo.rpc.naming.context.ServiceContextHolder;
import com.lyon.demo.rpc.naming.dispatcher.ProcessHandler;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public class InterfaceRegisterHandler implements ProcessHandler {
    @Override
    public Command execute(Command command) {
        //
        final Object deserialize = SerializeUtil.deserialize(command.getPayload());
        Pair<String,String> pair = (Pair<String,String>) deserialize;
        Assert.notNull(pair);
        // 注册服务
        ServiceContextHolder.addInterface(pair.getKey(),pair.getValue());
        final Command clone = command.clone();
        clone.setPayload(SerializeUtil.serialize(Result.success()));
        return clone;
    }

    @Override
    public int type() {
        return Types.NamingTypes.register_interface;
    }
}

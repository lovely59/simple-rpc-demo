package com.lyon.demo.rpc.naming.dispatcher.handler;

import cn.hutool.core.util.SerializeUtil;
import com.lyon.demo.rpc.api.core.Command;
import com.lyon.demo.rpc.api.core.Result;
import com.lyon.demo.rpc.api.core.Types;
import com.lyon.demo.rpc.naming.dispatcher.ProcessHandler;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Slf4j
public class ServiceBeatHandler implements ProcessHandler {
    @Override
    public Command execute(Command command) {
        // 反序列化
        log.info("收到心跳包..");
        // 心跳续期
        final Command clone = command.clone();
        clone.setPayload(SerializeUtil.serialize(Result.success()));
        return clone;
    }

    @Override
    public int type() {
        return Types.NamingTypes.beat;
    }
}

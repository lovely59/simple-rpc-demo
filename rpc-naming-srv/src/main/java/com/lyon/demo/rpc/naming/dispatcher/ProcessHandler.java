package com.lyon.demo.rpc.naming.dispatcher;

import com.lyon.demo.rpc.api.core.Command;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface ProcessHandler {

    Command execute(Command command);

    int type();

}

package com.lyon.demo.rpc.naming.dispatcher;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface ProcessHandlerRegister {

    void addProcess(int type, ProcessHandler processHandler);

    ProcessHandler getProcess(int type);

}

package com.lyon.demo.rpc.naming.dispatcher.handler;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.SerializeUtil;
import com.lyon.demo.rpc.api.core.Command;
import com.lyon.demo.rpc.api.core.Result;
import com.lyon.demo.rpc.api.core.ServiceInstances;
import com.lyon.demo.rpc.api.core.Types;
import com.lyon.demo.rpc.naming.context.ServiceContextHolder;
import com.lyon.demo.rpc.naming.dispatcher.ProcessHandler;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public class LookupInterfaceNameHandler implements ProcessHandler {
    @Override
    public Command execute(Command command) {
        //
        final Object deserialize = SerializeUtil.deserialize(command.getPayload());
        String interfaceName = (String) deserialize;
        Assert.notBlank(interfaceName);
        // 注册服务
        final ServiceInstances instances = ServiceContextHolder.getServiceByInterface(interfaceName);
        final Command clone = command.clone();
        clone.setPayload(SerializeUtil.serialize(Result.success(instances)));
        return clone;
    }

    @Override
    public int type() {
        return Types.NamingTypes.lookup_interface;
    }
}

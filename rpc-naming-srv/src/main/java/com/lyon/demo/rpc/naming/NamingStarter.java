package com.lyon.demo.rpc.naming;

import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public class NamingStarter {

    public static void main(String[] args) {
        NamingController controller = new NamingController();
        controller.start(new Properties());
    }

}

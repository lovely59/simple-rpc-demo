package com.lyon.demo.common.spi;

import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface SpiLoader<S> {

    /**
     * 加载spi
     *
     * @param clazz
     * @return clazzList
     */
    List<Class<S>> loader(Class<S> clazz);

    /**
     * 加载spi
     *
     * @param clazz    clazz
     * @param strategy 策略
     * @return clazz
     */
    Class<S> loader(Class<S> clazz, String strategy);

}

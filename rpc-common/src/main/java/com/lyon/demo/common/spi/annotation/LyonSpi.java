package com.lyon.demo.common.spi.annotation;

import java.lang.annotation.*;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface LyonSpi {

    /**
     * spi 加载的策略类型
     *
     * @return 策略类型
     */
    String value();

}

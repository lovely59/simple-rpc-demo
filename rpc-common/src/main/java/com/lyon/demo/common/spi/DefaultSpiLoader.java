package com.lyon.demo.common.spi;

import cn.hutool.core.text.CharSequenceUtil;
import com.lyon.demo.common.spi.annotation.LyonSpi;
import com.lyon.demo.common.spi.annotation.Signleton;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class DefaultSpiLoader {

    private static final Map<Class, Object> CACHE_CONTAINER = new ConcurrentHashMap<>();

    private static final Predicate<ServiceLoader.Provider> CACHE_PREDICATE = sProvider -> {
        Signleton annotation = (Signleton) sProvider.type().getAnnotation(Signleton.class);
        return Objects.isNull(annotation) || !CACHE_CONTAINER.containsKey(sProvider.type());
    };

    private static final BiPredicate<ServiceLoader.Provider, String> STRATEGY_PREDICATE = (sProvider, strategy) -> {
        if (Objects.isNull(strategy)) {
            return false;
        }
        LyonSpi annotation = (LyonSpi) sProvider.type().getAnnotation(LyonSpi.class);
        return Objects.nonNull(annotation) && CharSequenceUtil.equals(strategy, annotation.value());
    };


    public static <S> List<S> loader(Class<S> clazz) {
        return ServiceLoader
                .load(clazz)
                .stream()
                .filter(CACHE_PREDICATE)
                .map(DefaultSpiLoader::get)
                .collect(Collectors.toList());
    }

    public static <S> S loader(Class<S> clazz, String strategy) {
        return ServiceLoader
                .load(clazz)
                .stream()
                .filter(CACHE_PREDICATE)
                .filter(sProvider -> STRATEGY_PREDICATE.test(sProvider, strategy))
                .map(DefaultSpiLoader::get)
                .findFirst()
                .orElse(null);
    }

    private static <S> S get(ServiceLoader.Provider<S> sProvider) {
        S instance = sProvider.get();
        Signleton annotation = sProvider.type().getAnnotation(Signleton.class);
        if (Objects.nonNull(annotation)) {
            CACHE_CONTAINER.put(sProvider.type(), instance);
        }
        return instance;
    }

}

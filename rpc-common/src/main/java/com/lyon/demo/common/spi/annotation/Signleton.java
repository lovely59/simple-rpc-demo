package com.lyon.demo.common.spi.annotation;

import java.lang.annotation.*;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Documented
public @interface Signleton {
}

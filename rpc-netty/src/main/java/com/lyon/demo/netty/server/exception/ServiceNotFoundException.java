package com.lyon.demo.netty.server.exception;

/**
 * @author Lyon
 */
public class ServiceNotFoundException extends RuntimeException{

    public ServiceNotFoundException(String message) {
        super(message);
    }

    public ServiceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

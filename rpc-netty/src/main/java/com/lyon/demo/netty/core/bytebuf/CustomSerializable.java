package com.lyon.demo.netty.core.bytebuf;

/**
 * @author LeeYan9
 * @since 2022-04-18
 */
public interface CustomSerializable {

    /**
     * 长度
     * @return 长度
     */
    int length();

}

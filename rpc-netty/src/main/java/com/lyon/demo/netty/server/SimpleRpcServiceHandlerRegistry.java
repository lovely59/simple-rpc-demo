package com.lyon.demo.netty.server;

import cn.hutool.core.lang.Assert;
import com.lyon.demo.rpc.api.core.RpcServiceHandlerRegistry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Lyon
 */
public class SimpleRpcServiceHandlerRegistry implements RpcServiceHandlerRegistry {

    private static final Map<String, Object> SINGLETONS = new ConcurrentHashMap<>();

    @Override
    public <T> void registerService(Class<T> interfaceClass, T service) {
        Assert.notNull(interfaceClass);
        Assert.notNull(service);
        SINGLETONS.putIfAbsent(interfaceClass.getName(),service);
    }

    @Override
    public <T> T getService(String interfaceName) {
        Assert.notBlank(interfaceName);
        return (T) SINGLETONS.get(interfaceName);
    }
}

package com.lyon.demo.netty.core;

import com.lyon.demo.rpc.api.core.RespondFuture;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Lyon
 */
public class InFlightRequests {

    private Map<Long, RespondFuture> respondFutureMap = new ConcurrentHashMap<>();

    public void putRequest(RespondFuture respondFuture) {
        respondFutureMap.put(respondFuture.getRequestId(),respondFuture);
    }

    public RespondFuture get(long requestId) {
        return respondFutureMap.get(requestId);
    }

    public void remove(long requestId) {
        respondFutureMap.remove(requestId);
    }
}

package com.lyon.demo.rpc.api.core;

/**
 * @author Lyon
 */
public interface RpcServiceHandlerRegistry {

    <T> void registerService(Class<T> interfaceClass , T service);

    <T> T getService(String interfaceName);

}

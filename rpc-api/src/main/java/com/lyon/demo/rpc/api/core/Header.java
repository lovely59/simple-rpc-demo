package com.lyon.demo.rpc.api.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Header implements Serializable {

    private static final long serialVersionUID = 2258408911280315271L;
    private int type;

    private long requestId;

    private int version;

    public int length() {
        return Integer.BYTES + Integer.BYTES + Long.BYTES;
    }

}

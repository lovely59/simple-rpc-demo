package com.lyon.demo.rpc.api.core;

/**
 * @author Lyon
 */
public interface Types {

    interface NamingTypes{
        int register = 1;
        int lookup_service_name = 2;
        int lookup_interface = 3;
        int register_interface = 15;

        int beat = 4;

    }

    interface ClientTypes{
        int receive_method = 1;
    }

    interface ServerTypes{
        int exe_method = 1;
    }


}

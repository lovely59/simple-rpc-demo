package com.lyon.demo.rpc.api.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.lang.reflect.Type;

/**
 * @author Lyon
 */
@Data
@AllArgsConstructor
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = -6199666595989314557L;
    private String interfaceName;

    private String methodName;

    private Class<?>[] types;

    private byte[] args;
}

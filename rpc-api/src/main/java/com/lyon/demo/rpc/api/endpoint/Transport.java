package com.lyon.demo.rpc.api.endpoint;

import com.lyon.demo.rpc.api.core.Command;

import java.util.concurrent.CompletableFuture;

/**
 * @author Lyon
 */
public interface Transport {

    /**
     * 发送请求
     * @param request 请求command
     * @return 异步接收结果
     */
    CompletableFuture<Command> send(Command request);

}

package com.lyon.demo.rpc.api.endpoint;

import java.io.Closeable;
import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface RpcAccessEndpoint extends Closeable {

    /**
     * 获取接口（代理对象）
     * @param bizServiceType 业务接口
     * @param <T> 泛型
     * @return 接口（代理对象）
     */
    <T> T getRemoteService( Class<T> bizServiceType);

    <T> void registerProvider(Class<T> tClass, T obj);

    /**
     * 启动服务
     * @param properties ext
     * @return xx // TODO: 2022/4/15
     * @throws Exception 异常
     */
    Closeable startServer(Properties properties) throws Exception;
}









package com.lyon.demo.rpc.api.endpoint;


import com.lyon.demo.rpc.api.core.RpcServiceHandlerRegistry;

import java.net.SocketAddress;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface TransportServer {

    SocketAddress start(RpcServiceHandlerRegistry rpcServiceHandlerRegistry, int port) throws Exception;
    void stop();
}

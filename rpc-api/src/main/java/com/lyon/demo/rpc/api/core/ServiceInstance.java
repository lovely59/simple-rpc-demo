package com.lyon.demo.rpc.api.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.net.SocketAddress;

/**
 * @author Lyon
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceInstance implements Serializable {

    private static final long serialVersionUID = 1566858611474013277L;
    private String serviceName;

    private SocketAddress socketAddress;

    private Metadata metadata;

}

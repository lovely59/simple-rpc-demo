package com.lyon.demo.rpc.api.endpoint;

import java.net.SocketAddress;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface TransportClient {


    Transport createTransport(SocketAddress socketAddress, long connectTimeout ) throws InterruptedException;

}

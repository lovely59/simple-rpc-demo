package com.lyon.demo.rpc.api.core;

import lombok.Data;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Data
public class ServiceInstances implements Serializable {

    private static final long serialVersionUID = -1811630350807014989L;
    private String serviceName;

    private List<ServiceInstance> instances;

    public boolean active(){
        return instances != null && instances.size()>0;
    }

    public void addInstance(ServiceInstance serviceInstance){
        if (instances == null) {
            instances = new ArrayList<>();
        }
        instances.add(serviceInstance);
    }
}

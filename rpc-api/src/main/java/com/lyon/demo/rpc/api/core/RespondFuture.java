package com.lyon.demo.rpc.api.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.concurrent.CompletableFuture;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Data
@NoArgsConstructor
public class RespondFuture implements Serializable {

    private static final long serialVersionUID = 5420778988514821564L;
    private long requestId;

    private CompletableFuture<Command> future;

    private long timestamp;

    public RespondFuture(long requestId, CompletableFuture<Command> future) {
        this.requestId = requestId;
        this.future = future;
        this.timestamp = System.nanoTime();
    }
}

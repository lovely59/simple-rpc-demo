package com.lyon.demo.rpc.api.core;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.clone.Cloneable;
import cn.hutool.core.util.IdUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
@Data
@NoArgsConstructor
public class Command implements Cloneable<Command>, Serializable {

    private static final long serialVersionUID = -1705375858233116613L;

    private Header header;

    private byte[] payload;

    public int length() {
        return header.length() + (Objects.isNull(payload) ? 0 : payload.length);
    }

    public Command(int type, int version, byte[] payload) {
        this.header = new Header(type, IdUtil.getSnowflake().nextId(), version);
        this.payload = payload;
    }

    @Override
    public Command clone() {
        final Command backup = new Command();
        BeanUtil.copyProperties(this, backup);
        return backup;
    }
}

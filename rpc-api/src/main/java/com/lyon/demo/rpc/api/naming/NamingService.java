package com.lyon.demo.rpc.api.naming;

import com.lyon.demo.rpc.api.core.ServiceInstance;
import com.lyon.demo.rpc.api.core.ServiceInstances;

import java.io.Closeable;
import java.net.SocketAddress;
import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface NamingService {

    /**
     * 注册服务
     *
     * @param serviceInstance 服务
     */
    void registerService(ServiceInstance serviceInstance);

    /**
     * 获取服务对应的地址
     *
     * @param serviceName 服务名称
     * @return uri
     */
    ServiceInstances lookupService(String serviceName);

    <T> ServiceInstances lookupService(Class<T> interfaceType);



    Closeable start(SocketAddress uri, Properties prop);


    void registerInterface(String interfaceName, String serviceName);
}

package com.lyon.demo.rpc.api.core;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public class Metadata extends HashMap<String, String> implements Serializable {
    private static final long serialVersionUID = 4767137264390393294L;
}

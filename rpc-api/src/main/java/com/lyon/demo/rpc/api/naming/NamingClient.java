package com.lyon.demo.rpc.api.naming;

import com.lyon.demo.rpc.api.endpoint.Transport;

import java.net.SocketAddress;

/**
 * @author Lyon
 */
public interface NamingClient {

    Transport createTransport(SocketAddress socketAddress , long connectTimeout ) throws InterruptedException;

}

package com.lyon.demo.server;

import com.lyon.demo.common.spi.DefaultSpiLoader;
import com.lyon.demo.example.service.HelloService;
import com.lyon.demo.rpc.api.endpoint.RpcAccessEndpoint;
import com.lyon.demo.server.service.impl.HelloServiceImpl;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * @author Lyon
 */
@Slf4j
public class RpcServer {

    private static final String PROTOCOL = "netty";

    public static void main(String[] args) throws Exception {
        // 创建远程访问服务 （客户端，服务端，命名服务注册）
        final RpcAccessEndpoint rpcAccessEndpoint = DefaultSpiLoader.loader(RpcAccessEndpoint.class, PROTOCOL);
        Properties prop = initProp();
        rpcAccessEndpoint.startServer(prop);
        // 服务端注册接口
        HelloService helloService = new HelloServiceImpl();
        rpcAccessEndpoint.registerProvider(HelloService.class,helloService);
        log.info("RpcServer端-注册接口完成");
    }

    private static Properties initProp() {
        final Properties prop = new Properties();
        prop.setProperty("remote-protocol",PROTOCOL);
        prop.setProperty("naming-Protocol",PROTOCOL);
        prop.setProperty("naming-remote-protocol",PROTOCOL);
        prop.setProperty("serviceName","rpc-server-demo");
        prop.put("naming-addr", new InetSocketAddress("localhost",18880));
        prop.put("port",18881);
        return prop;
    }

}

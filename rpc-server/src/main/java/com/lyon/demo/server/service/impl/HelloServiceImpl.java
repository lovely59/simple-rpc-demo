package com.lyon.demo.server.service.impl;

import cn.hutool.core.util.StrUtil;
import com.lyon.demo.example.service.HelloService;

/**
 * @author Lyon
 */
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String username) {
        return StrUtil.format("Hello {} !!",username);
    }
}

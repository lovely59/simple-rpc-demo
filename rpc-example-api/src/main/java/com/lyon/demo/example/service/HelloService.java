package com.lyon.demo.example.service;

/**
 * @author Lyon
 */
public interface HelloService {

    String hello(String username);

}
